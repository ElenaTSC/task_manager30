package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IAbstractBusinessService<Project> {

    void create(String userId, String name, String description);

}
