package ru.tsk.ilina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();
}
