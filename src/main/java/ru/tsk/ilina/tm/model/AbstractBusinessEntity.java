package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractOwnerEntity {

    @Nullable
    private String name = "";
    @Nullable
    private String description = "";
    @Nullable
    private Status status = Status.NOT_STARTED;
    @Nullable
    private Date startDate;
    @Nullable
    private Date finishDate;
    @Nullable
    private Date createdDate = new Date();

    @Override
    public String toString() {
        return id + " : " + name;
    }

}
