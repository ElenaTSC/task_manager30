package ru.tsk.ilina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    private static final String COMMAND_SAVE = "backup-save";
    @NotNull
    private static final String COMMAND_LOAD = "backup-load";
    @NotNull
    private static final int INTERVAL = 30;
    @NotNull
    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.runCommand(COMMAND_SAVE);
    }

    public void load() {
        bootstrap.runCommand(COMMAND_LOAD);
    }
}
