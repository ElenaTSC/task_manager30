package ru.tsk.ilina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.dto.Domain;
import ru.tsk.ilina.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "data-load-bin";
    }

    @Override
    public @NotNull String description() {
        return "Load binary data";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
