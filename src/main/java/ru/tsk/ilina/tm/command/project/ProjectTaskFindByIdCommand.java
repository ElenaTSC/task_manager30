package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractProjectTaskCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectTaskFindByIdCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "project_task_find_by_id";
    }

    @NotNull
    @Override
    public String description() {
        return "Find task by project id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(userId, id);
        if (tasks == null) throw new TaskNotFoundException();
        for (Task task : tasks) System.out.println(task.toString());
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
