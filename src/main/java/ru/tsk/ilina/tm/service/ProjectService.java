package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.api.service.IAbstractBusinessService;
import ru.tsk.ilina.tm.api.service.IAuthService;
import ru.tsk.ilina.tm.api.service.IProjectService;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.empty.*;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.exception.system.IndexIncorrectException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractBusinessService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        repository.add(userId, project);
    }

}
